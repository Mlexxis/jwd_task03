package by.etc.jwd.task03.action.reader;

import by.etc.jwd.task03.action.exception.ReaderException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReaderTest {

    @Test (groups = {"reader"})
    public void read_shouldReadEntireFile() throws URISyntaxException {
        Path filepath = Paths.get(this.getClass().getClassLoader().getResource("input.txt").toURI());
        TextFileReader reader = new TextFileReader();

        try {
            String expected = "First P, first S. Second S...\nSecond P, third S. Fourth S!!!";
            String actual = reader.read(filepath);
            Assert.assertEquals(actual, expected);
        } catch (ReaderException e) {
            e.printStackTrace();
        }
    }
}
