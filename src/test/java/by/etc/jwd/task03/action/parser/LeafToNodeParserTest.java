package by.etc.jwd.task03.action.parser;

import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextLeaf;
import by.etc.jwd.task03.domain.TextNode;
import by.etc.jwd.task03.domain.TextUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LeafToNodeParserTest {

    @Test (groups = {"parser"})
    public void leafToNode_SplitToWords_ShouldReturnProperNode() {
        TextLeaf leaf = new TextLeaf();
        leaf.setItem("Java is Java");

        TextUnit expected = new TextNode();
        TextLeaf l01 = new TextLeaf();
        l01.setItem("Java");
        TextLeaf l02 = new TextLeaf();
        l02.setItem("is");
        TextLeaf l03 = new TextLeaf();
        l03.setItem("Java");
        expected.addItem(l01);
        expected.addItem(l02);
        expected.addItem(l03);

        try {
            TextUnit actual = LeafToNodeParser.getInstance().leafToNode(leaf, RegExps.TO_WORD.getRegex());
            Assert.assertEquals(actual, expected);
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }

    @Test (groups = {"parser"})
    public void leafToNode_SplitToSentences_ShouldReturnProperNode() {
        TextLeaf leaf = new TextLeaf();
        leaf.setItem("Java is Java. Java is Java... Java is Java? Java is Java!!!");

        TextUnit expected = new TextNode();
        TextLeaf l01 = new TextLeaf();
        l01.setItem("Java is Java.");
        TextLeaf l02 = new TextLeaf();
        l02.setItem("Java is Java...");
        TextLeaf l03 = new TextLeaf();
        l03.setItem("Java is Java?");
        TextLeaf l04 = new TextLeaf();
        l04.setItem("Java is Java!!!");
        expected.addItem(l01);
        expected.addItem(l02);
        expected.addItem(l03);
        expected.addItem(l04);

        try {
            TextUnit actual = LeafToNodeParser.getInstance().leafToNode(leaf, RegExps.TO_SENTENCE.getRegex());
            Assert.assertEquals(actual, expected);
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }

    @Test (groups = {"parser"})
    public void leafToNode_SplitToParagraphs_ShouldReturnProperNode() {
        TextLeaf leaf = new TextLeaf();
        leaf.setItem("Java is Java. Java is Java...\nJava is Java? Java is Java!!!");

        TextUnit expected = new TextNode();
        TextLeaf l01 = new TextLeaf();
        l01.setItem("Java is Java. Java is Java...");
        TextLeaf l02 = new TextLeaf();
        l02.setItem("Java is Java? Java is Java!!!");
        expected.addItem(l01);
        expected.addItem(l02);

        try {
            TextUnit actual = LeafToNodeParser.getInstance().leafToNode(leaf, RegExps.TO_PARAGRAPH.getRegex());
            Assert.assertEquals(actual, expected);
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }
}
