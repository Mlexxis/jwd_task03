package by.etc.jwd.task03.action.parser;

import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextLeaf;
import by.etc.jwd.task03.domain.TextUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AllParsersTest {

    @Test(groups = {"parser"})
    public void allParsers_ShouldReturnProperObject() {
        String input = "First P, first S. Second S.\nSecond P, third S. Fourth S.";
        TextLeaf text = new TextLeaf();
        text.setItem(input);

        Parsable<TextUnit> sentenceParser = new SentenceToLexemParser(RegExps.TO_WORD.getRegex());
        Parsable<TextUnit> paragraphParser = new ParagraphToSentenceParser(sentenceParser, RegExps.TO_SENTENCE.getRegex());
        Parsable<TextUnit> textParser = new TextToParagraphParser(paragraphParser, RegExps.TO_PARAGRAPH.getRegex());

        String expected = "First P, first S. Second S. Second P, third S. Fourth S.";

        try {
            TextUnit parsed = textParser.parse(text);
            String actual = parsed.composeText().trim();
            Assert.assertEquals(actual, expected);
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }
}
