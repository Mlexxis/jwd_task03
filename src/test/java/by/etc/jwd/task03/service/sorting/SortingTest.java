package by.etc.jwd.task03.service.sorting;

import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.action.parser.*;
import by.etc.jwd.task03.domain.TextLeaf;
import by.etc.jwd.task03.domain.TextNode;
import by.etc.jwd.task03.domain.TextUnit;
import by.etc.jwd.task03.service.TextUnitSorter;
import by.etc.jwd.task03.service.sorting.condition.SortByLexemLength;
import by.etc.jwd.task03.service.sorting.condition.SortByNumberOfSymbolInSentenceDescThenNatural;
import by.etc.jwd.task03.service.sorting.condition.SortBySentencesInParagraph;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SortingTest {

    @Test(groups = {"sorting"})
    public void SortByLexemLength() {
        TextUnit tree = new TextNode();
        String text = "aaaaaaxxxaa aaxaa. bb bbxbb. cccccccxxcccccc c.\ncddddd bddd addddddd.";
        TextLeaf leaf = new TextLeaf();
        leaf.setItem(text);

        Parsable<TextUnit> sentenceParser = new SentenceToLexemParser(RegExps.TO_WORD.getRegex());
        Parsable<TextUnit> paragraphParser = new ParagraphToSentenceParser(sentenceParser, RegExps.TO_SENTENCE.getRegex());
        Parsable<TextUnit> textParser = new TextToParagraphParser(paragraphParser, RegExps.TO_PARAGRAPH.getRegex());
        try {
            tree = textParser.parse(leaf);
        } catch (ParserException e) {
            e.printStackTrace();
        }

        TextLeaf l01 = new TextLeaf();
        TextLeaf l02 = new TextLeaf();
        TextLeaf l03 = new TextLeaf();
        TextLeaf l04 = new TextLeaf();
        TextLeaf l05 = new TextLeaf();
        TextLeaf l06 = new TextLeaf();
        TextLeaf l07 = new TextLeaf();
        TextLeaf l08 = new TextLeaf();
        TextLeaf l09 = new TextLeaf();
        TextNode n01 = new TextNode();
        TextNode n02 = new TextNode();
        TextNode n03 = new TextNode();
        TextNode n04 = new TextNode();
        TextNode pn01 = new TextNode();
        TextNode pn02 = new TextNode();
        l01.setItem("aaxaa.");
        l02.setItem("aaaaaaxxxaa");
        l03.setItem("bb");
        l04.setItem("bbxbb.");
        l05.setItem("c.");
        l06.setItem("cccccccxxcccccc");
        l07.setItem("bddd");
        l08.setItem("cddddd");
        l09.setItem("addddddd.");

        n01.addItem(l01);
        n01.addItem(l02);
        n02.addItem(l03);
        n02.addItem(l04);
        n03.addItem(l05);
        n03.addItem(l06);
        pn01.addItem(n01);
        pn01.addItem(n02);
        pn01.addItem(n03);
        n04.addItem(l07);
        n04.addItem(l08);
        n04.addItem(l09);
        pn02.addItem(n04);
        TextUnit expected = new TextNode();
        expected.addItem(pn01);
        expected.addItem(pn02);

        TextUnit actual = TextUnitSorter.sort(tree, new SortByLexemLength());
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"sorting"})
    public void SortByNumberOfSymbolInSentenceDescThenNatural() {
        TextUnit tree = new TextNode();
        String text = "aaaaaaxxxaa aaxaa. bb bbxbb. cccccccxxcccccc c.\ncddddd bddd addddddd.";
        TextLeaf leaf = new TextLeaf();
        leaf.setItem(text);

        Parsable<TextUnit> sentenceParser = new SentenceToLexemParser(RegExps.TO_WORD.getRegex());
        Parsable<TextUnit> paragraphParser = new ParagraphToSentenceParser(sentenceParser, RegExps.TO_SENTENCE.getRegex());
        Parsable<TextUnit> textParser = new TextToParagraphParser(paragraphParser, RegExps.TO_PARAGRAPH.getRegex());
        try {
            tree = textParser.parse(leaf);
        } catch (ParserException e) {
            e.printStackTrace();
        }

        TextLeaf l01 = new TextLeaf();
        TextLeaf l02 = new TextLeaf();
        TextLeaf l03 = new TextLeaf();
        TextLeaf l04 = new TextLeaf();
        TextLeaf l05 = new TextLeaf();
        TextLeaf l06 = new TextLeaf();
        TextLeaf l07 = new TextLeaf();
        TextLeaf l08 = new TextLeaf();
        TextLeaf l09 = new TextLeaf();
        TextNode n01 = new TextNode();
        TextNode n02 = new TextNode();
        TextNode n03 = new TextNode();
        TextNode n04 = new TextNode();
        TextNode pn01 = new TextNode();
        TextNode pn02 = new TextNode();
        l01.setItem("aaxaa.");
        l02.setItem("aaaaaaxxxaa");
        l03.setItem("bb");
        l04.setItem("bbxbb.");
        l05.setItem("c.");
        l06.setItem("cccccccxxcccccc");
        l07.setItem("bddd");
        l08.setItem("cddddd");
        l09.setItem("addddddd.");

        n01.addItem(l01);
        n01.addItem(l02);
        n02.addItem(l03);
        n02.addItem(l04);
        n03.addItem(l05);
        n03.addItem(l06);
        pn01.addItem(n01);
        pn01.addItem(n02);
        pn01.addItem(n03);
        n04.addItem(l09);
        n04.addItem(l07);
        n04.addItem(l08);
        pn02.addItem(n04);
        TextUnit expected = new TextNode();
        expected.addItem(pn01);
        expected.addItem(pn02);

        TextUnit actual = TextUnitSorter.sort(tree, new SortByNumberOfSymbolInSentenceDescThenNatural('x'));
        Assert.assertEquals(actual, expected);

    }

    @Test(groups = {"sorting"})
    public void SortBySentencesInParagraph() {
        TextUnit tree = new TextNode();
        String text = "aaaaaaxxxaa aaxaa. bb bbxbb. cccccccxxcccccc c.\ncddddd bddd addddddd.";
        TextLeaf leaf = new TextLeaf();
        leaf.setItem(text);

        Parsable<TextUnit> sentenceParser = new SentenceToLexemParser(RegExps.TO_WORD.getRegex());
        Parsable<TextUnit> paragraphParser = new ParagraphToSentenceParser(sentenceParser, RegExps.TO_SENTENCE.getRegex());
        Parsable<TextUnit> textParser = new TextToParagraphParser(paragraphParser, RegExps.TO_PARAGRAPH.getRegex());
        try {
            tree = textParser.parse(leaf);
        } catch (ParserException e) {
            e.printStackTrace();
        }

        TextLeaf l01 = new TextLeaf();
        TextLeaf l02 = new TextLeaf();
        TextLeaf l03 = new TextLeaf();
        TextLeaf l04 = new TextLeaf();
        TextLeaf l05 = new TextLeaf();
        TextLeaf l06 = new TextLeaf();
        TextLeaf l07 = new TextLeaf();
        TextLeaf l08 = new TextLeaf();
        TextLeaf l09 = new TextLeaf();
        TextNode n01 = new TextNode();
        TextNode n02 = new TextNode();
        TextNode n03 = new TextNode();
        TextNode n04 = new TextNode();
        TextNode pn01 = new TextNode();
        TextNode pn02 = new TextNode();
        l01.setItem("aaxaa.");
        l02.setItem("aaaaaaxxxaa");
        l03.setItem("bb");
        l04.setItem("bbxbb.");
        l05.setItem("c.");
        l06.setItem("cccccccxxcccccc");
        l07.setItem("bddd");
        l08.setItem("cddddd");
        l09.setItem("addddddd.");

        n01.addItem(l08);
        n01.addItem(l07);
        n01.addItem(l09);

        pn01.addItem(n01);

        n02.addItem(l02);
        n02.addItem(l01);

        n03.addItem(l03);
        n03.addItem(l04);

        n04.addItem(l06);
        n04.addItem(l05);

        pn02.addItem(n02);
        pn02.addItem(n03);
        pn02.addItem(n04);

        TextUnit expected = new TextNode();
        expected.addItem(pn01);
        expected.addItem(pn02);

        TextUnit actual = TextUnitSorter.sort(tree, new SortBySentencesInParagraph());
        Assert.assertEquals(actual, expected);
    }
}
