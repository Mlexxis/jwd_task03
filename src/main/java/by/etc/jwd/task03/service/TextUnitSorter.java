package by.etc.jwd.task03.service;

import by.etc.jwd.task03.domain.TextUnit;
import by.etc.jwd.task03.service.sorting.condition.SortCondition;

public class TextUnitSorter {

    public static TextUnit sort(TextUnit textUnit, SortCondition condition) {
        condition.sortTextUnit(textUnit);
        return textUnit;
    }
}
