package by.etc.jwd.task03.service.sorting.condition;

import by.etc.jwd.task03.domain.TextUnit;

import java.util.Comparator;

public class SortByLexemLength implements SortCondition {


    @Override
    public void sortTextUnit(TextUnit textUnit) {
        for (TextUnit paragraph : textUnit.items()) {
            for (TextUnit sentence : paragraph.items()) {
                sentence.items().sort(Comparator.comparingInt(TextUnit::length));
            }
        }
    }
}
