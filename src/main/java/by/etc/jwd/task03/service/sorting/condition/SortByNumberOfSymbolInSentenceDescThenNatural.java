package by.etc.jwd.task03.service.sorting.condition;

import by.etc.jwd.task03.domain.TextUnit;

import java.util.Comparator;

public class SortByNumberOfSymbolInSentenceDescThenNatural implements SortCondition {

    private char symbol;

    public SortByNumberOfSymbolInSentenceDescThenNatural(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public void sortTextUnit(TextUnit textUnit) {
        for (TextUnit paragraph : textUnit.items()) {
            for (TextUnit sentence : paragraph.items()) {
                sentence.items()
                        .sort(((Comparator<TextUnit>) (lexem02, lexem01) -> symbolQuantity(lexem02.item(), symbol) - symbolQuantity(lexem01.item(), symbol))
                        .thenComparing((lexem01, lexem02) -> lexem01.item().compareTo(lexem02.item())));
            }
        }
    }

    private int symbolQuantity(String word, char symbol) {
        int counter = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol) {
                counter++;
            }
        }
        return counter;
    }
}
