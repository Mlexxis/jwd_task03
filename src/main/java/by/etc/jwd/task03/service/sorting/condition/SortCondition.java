package by.etc.jwd.task03.service.sorting.condition;

import by.etc.jwd.task03.domain.TextUnit;

public interface SortCondition {

    void sortTextUnit(TextUnit textUnit);
}
