package by.etc.jwd.task03.service.sorting.condition;

import by.etc.jwd.task03.domain.TextUnit;

import java.util.Comparator;

public class SortBySentencesInParagraph implements SortCondition {


    @Override
    public void sortTextUnit(TextUnit textUnit) {
        textUnit.items().sort(Comparator.comparingInt(TextUnit::length));
    }
}
