package by.etc.jwd.task03.action.exception;

public class ParserException extends Exception {

    public ParserException() {
    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Exception ex) {
        super(message, ex);
    }

    public ParserException(Exception ex) {
        super(ex);
    }
}
