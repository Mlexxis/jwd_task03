package by.etc.jwd.task03.action.reader;

import by.etc.jwd.task03.action.exception.ReaderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class TextFileReader {

    private final Logger log = LogManager.getRootLogger();

    public String read(Path uri) throws ReaderException {
        if (uri == null) {
            throw new ReaderException("Path is null");
        }

        String result;

        try {
            result = Files.lines(uri).collect(Collectors.joining("\n"));
        } catch (IOException e) {
            log.error("File not found");
            throw new ReaderException("File not found", e);
        }
        return result;
    }
}
