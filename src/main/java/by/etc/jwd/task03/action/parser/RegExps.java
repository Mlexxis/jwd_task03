package by.etc.jwd.task03.action.parser;

public enum RegExps {
    TO_WORD {
        @Override
        public String getRegex() {
            return "(?<=[\\s]+)";
        }
    }, TO_SENTENCE {
        @Override
        public String getRegex() {
            return "(?<=[.!?]{1,10} )";
        }
    }, TO_PARAGRAPH {
        @Override
        public String getRegex() {
            return "(?=[\\n])";
        }
    };

    public abstract String getRegex();
}
