package by.etc.jwd.task03.action.parser;


import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextUnit;

public class TextToParagraphParser implements Parsable<TextUnit>{

    private Parsable<TextUnit> nextParser;
    private String regex;

    public TextToParagraphParser(Parsable<TextUnit> nextParser, String regex) {
        this.nextParser = nextParser;
        this.regex = regex;
    }

    @Override
    public TextUnit parse(TextUnit textLeaf) throws ParserException {
        TextUnit textNode = LeafToNodeParser.getInstance().leafToNode(textLeaf, regex);
        if (nextParser != null) {
            return nextParser.parse(textNode);
        } else return textNode;
    }
}
