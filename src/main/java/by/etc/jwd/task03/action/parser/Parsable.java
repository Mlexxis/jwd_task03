package by.etc.jwd.task03.action.parser;

import by.etc.jwd.task03.action.exception.ParserException;

public interface Parsable<T> {

    T parse(T source) throws ParserException;
}
