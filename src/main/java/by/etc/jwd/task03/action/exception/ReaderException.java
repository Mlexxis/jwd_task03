package by.etc.jwd.task03.action.exception;

public class ReaderException extends Exception{
    public ReaderException() {
    }

    public ReaderException(String message) {
        super(message);
    }

    public ReaderException(String message, Exception ex) {
        super(message, ex);
    }

    public ReaderException(Exception ex) {
        super(ex);
    }
}
