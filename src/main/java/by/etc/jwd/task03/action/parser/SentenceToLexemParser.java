package by.etc.jwd.task03.action.parser;


import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextNode;
import by.etc.jwd.task03.domain.TextUnit;

public class SentenceToLexemParser implements Parsable<TextUnit> {

    private String regex;

    public SentenceToLexemParser(String regex) {
        this.regex = regex;
    }

    @Override
    public TextUnit parse(TextUnit sentenceLeaf) throws ParserException {
        TextUnit sentenceNode = new TextNode();
        for (TextUnit paragraph : sentenceLeaf.items()) {
            TextUnit paragraphNode = new TextNode();
            for (TextUnit sentence : paragraph.items()) {
                paragraphNode.addItem(LeafToNodeParser.getInstance().leafToNode(sentence, regex));
            }
            sentenceNode.addItem(paragraphNode);
        }
        return sentenceNode;
    }
}
