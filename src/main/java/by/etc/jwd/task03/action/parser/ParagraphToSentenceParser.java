package by.etc.jwd.task03.action.parser;


import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextNode;
import by.etc.jwd.task03.domain.TextUnit;

public class ParagraphToSentenceParser implements Parsable<TextUnit> {

    private Parsable<TextUnit> nextParser;
    private String regex;

    public ParagraphToSentenceParser(Parsable<TextUnit> nextParser, String regex) {
        this.nextParser = nextParser;
        this.regex = regex;
    }

    @Override
    public TextUnit parse(TextUnit paragraphLeaf) throws ParserException {
        TextUnit paragraphNode = new TextNode();
        for (TextUnit paragraph : paragraphLeaf.items()) {
            paragraphNode.addItem(LeafToNodeParser.getInstance().leafToNode(paragraph, regex));
        }
        if (nextParser != null) {
            return nextParser.parse(paragraphNode);
        } else return paragraphNode;
    }
}
