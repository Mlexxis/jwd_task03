package by.etc.jwd.task03.action.parser;


import by.etc.jwd.task03.action.exception.ParserException;
import by.etc.jwd.task03.domain.TextLeaf;
import by.etc.jwd.task03.domain.TextNode;
import by.etc.jwd.task03.domain.TextUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LeafToNodeParser {

    private final Logger log = LogManager.getRootLogger();

    private LeafToNodeParser() {
    }

    public static LeafToNodeParser getInstance() {
        return Nested.instance;
    }

    public TextUnit leafToNode(TextUnit source, String regex) throws ParserException {
        TextUnit target = new TextNode();
        try {
            String[] tmp = source.item().split(regex);
            for (String s : tmp) {
                TextLeaf leaf = new TextLeaf();
                leaf.setItem(s.trim());
                target.addItem(leaf);
            }
        } catch (UnsupportedOperationException ex) {
            log.error("parameter textUnit object is no a Leaf", ex);
            throw new ParserException("parameter textUnit object is no a Leaf", ex);
        }
        return target;
    }

    private static class Nested {
        static LeafToNodeParser instance = new LeafToNodeParser();
    }
}
