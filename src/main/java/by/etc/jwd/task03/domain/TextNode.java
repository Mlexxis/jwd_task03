package by.etc.jwd.task03.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TextNode implements Serializable, TextUnit {

    private static final long serialVersionUID = -7304866233294474296L;
    private List<TextUnit> items;

    public TextNode() {
        items = new ArrayList<>();
    }

    @Override
    public void addItem(TextUnit item) {
        items.add(item);
    }

    @Override
    public void removeItem(TextUnit item) {
        items.remove(item);
    }

    @Override
    public List<TextUnit> items() {
        return items;
    }

    public int length() {
        return items.size();
    }

    public String composeText() {
        StringBuilder sb = new StringBuilder();
        for (TextUnit element : items) {
            sb.append(element.composeText());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        TextNode anotherNode = (TextNode) obj;
        return this.items.equals(anotherNode.items);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (this.items == null ? 0 : items.hashCode());
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TextNode{");
        sb.append("items=").append(items);
        sb.append('}');
        return sb.toString();
    }
}
