package by.etc.jwd.task03.domain;

import java.io.Serializable;

public class TextLeaf implements Serializable, TextUnit {

    private static final long serialVersionUID = -3940595510932467316L;
    private static final String WHITESPACE = " ";
    private String item;


    public TextLeaf() {
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String item() {
        return item;
    }

    @Override
    public void addItem(TextUnit textUnit) {
        item = textUnit.item();
    }

    public int length() {
        return item.length();
    }

    public String composeText() {
        return item.concat(WHITESPACE);
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        TextLeaf anotherLeaf = (TextLeaf) obj;
        if (this.item == null && anotherLeaf.item == null) {
            return true;
        } else return this.item.equals(anotherLeaf.item);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (this.item == null ? 0 : item.hashCode());
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TextLeaf{");
        sb.append("item='").append(item).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
