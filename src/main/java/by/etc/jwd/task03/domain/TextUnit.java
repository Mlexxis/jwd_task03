package by.etc.jwd.task03.domain;

import java.util.Collections;
import java.util.List;

public interface TextUnit {

    default List<TextUnit> items() {
        return Collections.emptyList();
    }

    default String item() {
        throw new UnsupportedOperationException();
    }

    default void removeItem(TextUnit item) {
        throw new UnsupportedOperationException();
    }

    default void addItem(TextUnit item) {
        throw new UnsupportedOperationException();
    }

    int length();
    String composeText();




}
